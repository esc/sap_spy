#!/usr/bin/perl
# This script listens to a given multicast address for SAP/SDP packets and
# rebroadcasts them on another after sanitizing the SDP packets to VBrick's 
# liking. 
# ESC 2009.07.01

use warnings;
use strict;
use Net::SAP;
use Net::SDP;
use Getopt::Long;

# set up some globals
# Listen on the IPv4-global multicast addr.
my $l_group = Net::SAP->new('ipv4-global');
my $b_group = Net::SAP->new('224.2.125.254');

sub print_sdp {
    # This subroutine prints various SDP info to a file.  It's mostly for 
    # debugging.
    my $sdp = shift;

    # The filename we want to dump our text to.
    my $filename = 'capture.txt';

    open(FH, '>', $filename);
    my $time = $sdp->new_time_desc();
    my $vid = $sdp->media_desc_of_type('video');
    my $aud = $sdp->media_desc_of_type('audio');
    my $name = $sdp->session_name();
    my $info = $sdp->session_info();

    print FH "Sess  name: $name\n";
    print FH "Sess  info: $info\n";
    print FH "Sess orign: ".$sdp->session_origin()."\n";
    print FH "Bdcst time: ".$time->as_string()."\n";

    if(defined $vid) {
        print FH "Media type: ".$vid->media_type()."\n";
        print FH "Media addr: ".$vid->address()." port: ".$vid->port()."\n";
    }
    if(defined $aud) {
        print FH "Media type: ".$aud->media_type()."\n";
        print FH "Media addr: ".$aud->address()." port: ".$aud->port()."\n";
    }
    close FH;
}

sub send_sanitized_sdp {
    # Build a fresh sdp from the data we have and send it out via our
    # secondary SAP group.
    my $sdp = shift;
    my $sap = shift;
    my $verbose = shift;

    my $golden_sdp = Net::SDP->new();

    # set up o= like VBrick wants
    $golden_sdp->session_origin_username('-');
    $golden_sdp->session_origin_id(100000000);
    $golden_sdp->session_origin_version(4);
    $golden_sdp->session_origin_net_type('IN');
    $golden_sdp->session_origin_addr_type('IP4');
    $golden_sdp->session_origin_address($sdp->session_origin_address());

    # set up s= like VBrick wants
    $golden_sdp->session_name($sdp->session_name());
    if($verbose) {
        print "Name: ".$sdp->session_name()."\n";
    }
    
    # set up i= like VBrick wants
    $golden_sdp->session_info('VBrick Streaming Video');

    # set up m=, c=, and a= like VBrick wants
    my $vid = $sdp->media_desc_of_type('video');
    if(defined $vid) {
        my $golden_vid = $golden_sdp->new_media_desc('video');
        $golden_vid->network_type('IN');
        $golden_vid->address_type('IP4');
        $golden_vid->address($vid->address());
        $golden_vid->port($vid->port());
        if($verbose) {
            print "Media addr: ".$vid->address()." port: ".$vid->port()."\n";
        }
        $golden_vid->ttl(64);
        $golden_vid->transport('udp');
        $golden_vid->attribute('packetsize', 1316);
        $golden_vid->attribute('packetformat', 'RAW');
        $golden_vid->attribute('mux','m1s');
        $golden_vid->attribute('keywds','');
        $golden_vid->attribute('author','N13487');
        $golden_vid->attribute('copyright','2001, My Company');
        $golden_vid->default_format_num(33);
    }

    # set up t= like VBrick wants
    my $time = $golden_sdp->new_time_desc();
    $time->make_permanent();

    $sap->send($golden_sdp);
}

sub INT_handler {
    # gracefully handle ctrl+c
    print "Stopping gracefully...\n";
    $l_group->close();
    $b_group->close();
    
    exit(0);
}

sub usage {
    print "Usage: $0 [options]\n";
    print "--help       Print this help message.\n";
    print "--verbose    Print information as the program runs.\n";
}

sub main {
    # Main function of the script.
    my $verbose = 0;
    my $usage = 0;
    my $arguments = GetOptions( "verbose" => \$verbose,
                                "help"    => \$usage  );

    # if the user requested help, print the usage message and quit
    if($usage) {
        &usage();
        exit(0);
    }

    # Clean up when we get a SIGINT
    $SIG{INT} = \&INT_handler;
    
    # run until we're KILLed
    while(1) {
        # Grab a SAP packet.
        my $packet = $l_group->receive();

        # Send on our SDPs for reprocessing.
        if($packet->type() eq 'advertisement' && 
           $packet->payload_type() eq 'application/sdp') {
            my $payload = Net::SDP->new($packet);
            &send_sanitized_sdp($payload,$b_group,$verbose);
        }
    }
}

&main(@ARGV);
