#!/usr/bin/perl
# This script forges SAP packets for a given multicast address.
# ESC 2009.07.13

use warnings;
use strict;
use Net::SAP;
use Net::SDP;
use IO::Socket::Multicast;

sub usage {
    # Prints a helpful usage message to stdout.
    print "Usage: $0 sess_name vid_ip vid_port\n";
    print "sess_name        The name of your session enclosed in quotes.\n";
    print "vid_ip           The IP address of your video stream.\n";
    print "vid_port         The port of your video stream.\n";
    print "All arguments required.\n";
}

sub send_sanitized_sdp {
    # generates a fresh SDP that VBrick can handle, then populates it with
    # the correct info
    my ($sap, $sess_name, $vid_ip, $vid_port, $random_ip) = @_; 
    my $golden_sdp = Net::SDP->new();

    # set up o= like VBrick wants
    $golden_sdp->session_origin_username('-');
    $golden_sdp->session_origin_id(100000000);
    $golden_sdp->session_origin_version(4);
    $golden_sdp->session_origin_net_type('IN');
    $golden_sdp->session_origin_addr_type('IP4');
    $golden_sdp->session_origin_address("181.228.1.".$random_ip);

    # set up s= like VBrick wants
    $golden_sdp->session_name($sess_name);
    
    # set up i= like VBrick wants
    $golden_sdp->session_info('VBrick Streaming Video');

    # set up m=, c=, and a= like VBrick wants
    my $golden_vid = $golden_sdp->new_media_desc('video');
    $golden_vid->network_type('IN');
    $golden_vid->address_type('IP4');
    $golden_vid->address($vid_ip);
    $golden_vid->port($vid_port);
    $golden_vid->ttl(64);
    $golden_vid->transport('udp');
    $golden_vid->attribute('packetsize', 1316);
    $golden_vid->attribute('packetformat', 'RAW');
    $golden_vid->attribute('mux','m1s');
    $golden_vid->attribute('keywds','');
    $golden_vid->attribute('author','N13487');
    $golden_vid->attribute('copyright','2001, My Company');
    $golden_vid->default_format_num(33);

    # set up t= like VBrick wants
    my $time = $golden_sdp->new_time_desc();
    $time->make_permanent();

    $sap->send($golden_sdp);
}

sub INT_handler {
    # gracefully handle ctrl+c
    print "Stopping gracefully...\n";
    $l_group->close();
    $b_group->close();
    
    exit(0);
}

sub main {
    # Main function of the script.
    my ($sess_name, $vid_ip, $vid_port) = @_;

    # Clean up when we get a SIGINT
    $SIG{INT} = \&INT_handler;

    # Listen on the IPv4-global multicast addr.
    my $sap = Net::SAP->new('ipv4-global');
    my $random_ip = int(rand(250));

    while(1) {
        &send_sanitized_sdp($sap,$sess_name,$vid_ip,$vid_port,$random_ip);
        sleep(5);
    }
}

if($#ARGV < 2) {
    &usage();
    exit(1);
}
else {
    &main(@ARGV);
}
